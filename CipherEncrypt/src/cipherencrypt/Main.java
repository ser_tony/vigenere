
package cipherencrypt;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author tony
 */
public class Main {

    /**
     * @param args the command line arguments
     */

    private static boolean isDebug = false;

    /**
     * Purpose of this application is ecnryption of plain text to
     * Vigenere cipher
     *
     * Input parameters of console applications:
     * 0 - <file_path> - path to text file which content will encrypted
     * 1 - <key> -will be used in encryption process
     *
     * In case of empty parameters  or nothing passed  exception will
     * be thrown
     */
    public static void main(String[] args) {
        
        String filePath, key;
        BufferedReader fin = null;
        PrintWriter outWriter = null;
        try{
           if (!Main.isDebug && args.length < 2) {
               throw new Exception("You should passing 2 parameters" +
                                   ": \n\r 0 - <file_path>\n\r 1- <key>");
           }
           else {
               filePath = args[0]; // "/home/tony/doran.txt";
               key = args[1];//"distance";
           }
           
           fin = new BufferedReader(new InputStreamReader(new
                   FileInputStream(filePath)));
           String content = "",
                   line = "";
           while ((line = fin.readLine()) != null) {
               content += line;
           }
           
           CipherVigenere cVeg = new CipherVigenere(content, key);

           //TODO: needed always to switch - just encrypting content by key
           String encryptText = cVeg.encrypt();
           File fOrigin = new File(filePath);
           System.out.println("Name: "+ fOrigin.getName());
           System.out.println("Absolute path: "+ fOrigin.getAbsolutePath());


           String origName = fOrigin.getName();
           //String[] nameParts = fOrigin.getName().split(".");
           String encName = origName.substring(0, origName.lastIndexOf('.'))
                   + "_encrypted" + origName.substring(origName.lastIndexOf('.'),
                   origName.length());
           encName = fOrigin.getParentFile().getAbsolutePath()
                   + File.separator + encName;
           
           fOrigin = null;

           outWriter = new PrintWriter(new File(encName));
           outWriter.print(encryptText);
        }
        catch (Exception ex) {
            System.out.println(ex);
            System.exit(1);
        }
        finally {
            if (fin!=null) {
                try {
                    fin.close();
                    outWriter.close();
                } catch (IOException ex) {
                    System.out.print(ex);
                }
            }
        }
    }

}
