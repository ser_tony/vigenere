
package cipherencrypt;

import java.util.ArrayList;
/**
 *
 * @author tony
 */

/**
 *  This class Cipher Vigenere realizing cipher
 *  of  English text in basic method Vigenere with key
 */
public class CipherVigenere {

    /** the key */
    protected String keyword;

    /** additional fields */
    protected String originalText;
    protected String cipherText;


    public CipherVigenere(String text, String key){
        this.originalText = text.toLowerCase().trim();
        this.keyword = key.toLowerCase().trim();
    }
    
    public String getOriginalText(){
        return this.originalText;
    }

    public String getChipherText() {
        return this.cipherText;
    }

    /**
    * @return encrypted text using Vigenere cipher
    */
    public String encrypt() {
        
        int originLength = this.originalText.length();
        int keyLength = this.keyword.length();
        char[] keyChars = this.keyword.toCharArray();
        int k = 0;

        //array with encrypted characters
        char[] encryptText = new char[originLength];
        
        for (int i = 0; i< originLength; i++) {
            char tempChar = this.originalText.charAt(i);
            if (Character.isLetter(tempChar)){
                //computing a offset of original text characters
                //relatively characters of key
                int diffOffset = ((int) tempChar + (int) keyChars[k % keyLength]
                                - 2* (int)'a') % 26;
                diffOffset = diffOffset < 0? diffOffset + 26: diffOffset;
                int offset =  diffOffset + (int)'a';
               encryptText[i] =   (char) offset;
               k += 1;
            }
            else {
                encryptText[i] = tempChar;
            }
        }
       this.cipherText = String.valueOf(encryptText);
       return this.cipherText;
    }

    /**
     * @return decrypted text using reverse using Vigenere
     * with defined key
     */
    public String decrypt(String encryptedText) {
        
        encryptedText = encryptedText.trim().toLowerCase();

        int encLength = encryptedText.length();
        int keyLength = this.keyword.length();
        int k = 0;
        char[] keyChars =   this.keyword.toCharArray();
        char[] plainText = new char[encLength];
        char[] processChars =  encryptedText.toCharArray();
        for( int i = 0; i< encLength; i++) {
            char tempChar = (char) processChars[i];
            if (Character.isLetter(tempChar)) {
                int diffOffset = ((int) tempChar - (int)keyChars[k % keyLength]) % 26;
                diffOffset = diffOffset < 0? diffOffset + 26: diffOffset;
                int offset = diffOffset + (int)'a';
                plainText[i] = (char) offset;
                k += 1;
            }
            else {
                plainText[i] = tempChar;
            }
        }
        this.originalText = String.valueOf(plainText);
        return this.originalText;
    }

}
