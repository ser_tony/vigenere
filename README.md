# README #

##1. CipherEncrypt 
- accessory console application, which purpose is ecnryption of plain text to Vigenere cipher. You can use it in command line: pass to directory with .jar (/dist), example:  
 **tony@tony-ser:~/Desktop/vigenere_task_seregin_a/CipherEncrypt/dist$ java -jar CipherEncrypt.jar ~/Desktop/vigenere_task_seregin_a/CipherEncrypt/some_oracle.txt**

mirror,
here first  parameter is path to text file content will encrypted, second - key will be used in encryption process.
Result is new file with ciphertext which has name as some_oracle_encrypted.txt located in the same folder of origin file

## 2. DecryptVigenere  - console application which decrypt ciphertext encrypted
   by Vigenere cipher and return result to console out.
  Way to use:
  tony@tony-ser:~/Desktop/vigenere_task_seregin_a/DecryptVigenere/dist$ java -jar DecryptVigenere.jar ../test/files/some_oracle_encrypted.txt
Plain text:
oracle offers a broad portfolio of software and hardware products and services that enable public, private, and hybrid clouds. oracle offers a broad portfolio of software and hardware products and services that enable public, private, and hybrid clouds. oracle offers a broad portfolio of software and hardware products and services that enable public, private, and hybrid clouds. oracle offers a broad portfolio of software and hardware products and services that enable public, private, and hybrid clouds.

##Key: javacore

You may specify path to encrypted file, for example from folder test/files.
On out you get possible decrypted plain text and key.  Accuracy of decrypting  depend of length ciphertext. In purpose getting more adequate decrypting results,  algorithm was improved by checking word sequences in english dictionary of yandex  service.

Test cases were located in directory of project: **test/decryptvigenere/HackVigenereTest.java**.