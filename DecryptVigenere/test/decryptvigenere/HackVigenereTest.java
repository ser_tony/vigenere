/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package decryptvigenere;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author tony
 */
public class HackVigenereTest {

    /**
     * test cases :
     *  [0] - file path to ciphertext sequences; (in lower case)
     *  [1] - file path to original text
     *  [2] - key which was used in produce of ciphertext
     */
    private String folder = "test/files/";

    String[][] cases = {
        {
            "google_content_encrypted.txt",
            "google_content.txt",
            "member"
        },
        {
            "doran_encrypted.txt",
            "doran.txt",
            "distance"
        },
        {
            "robot_encrypted.txt",
            "robot.txt",
            "super"
        },
        {
            /**This text really pretty short therefore if you wish get proper decoding
             * better repeat the same text some times, and i did it is
             */
            "some_oracle_encrypted.txt",
            "some_oracle.txt",
            "javacore"
        },
        {
            "short_encrypted.txt",
            "short.txt",
            "one"
        },
        {
            "shortest_encrypted.txt",
            "shortest.txt",
            "two"
        },
        {
            /** with maximum length key */
            "big_text_encrypted.txt",
            "big_text.txt",
            "independence"
        }
    };

    public HackVigenereTest() {
    }

    @Before
    public void setUp() {
        
    }

    @BeforeClass
    public static void setUpClass() throws Exception {
    }

    @AfterClass
    public static void tearDownClass() throws Exception {
    }

    /**
     * Test of hackText method, of class HackVigenere.
     */
    @Test
    public void testHackText_int_int() {
        System.out.println("hackText");
        int minLength = 2;
        int maxLength = 12;
        int tIndex = 1;
        
        for (String[] tempCase: this.cases) {
           String encText = this.readFile(tempCase[0]);
           String expText = this.readFile(tempCase[1]);
           expText = expText.toLowerCase().trim();
           String expKey = tempCase[2];
           
           HackVigenere instance  = new HackVigenere(encText);
           String[] result = instance.hackText(minLength, maxLength);

           System.out.println("Test [" + tIndex + "] , text length - " + expText.length());

           assertEquals("Test " + tIndex + " failed (decrypted incorrect)",  expText, result[0]);
           assertEquals("Test " + tIndex + " failed (key not equals)",  expKey, result[1]);
           tIndex ++;
        }
        
    }

    private String readFile(String filePath) {
       filePath = this.folder + filePath;
       BufferedReader fin = null;
       String content = "";
       try {
            fin = new BufferedReader(new InputStreamReader(new FileInputStream(filePath)));
            String line = "";
            
            while ((line = fin.readLine()) != null) {
                content += line;
            }

        } catch (Exception ex) {
            fail("" + ex);
        }
        finally {
            try {
                fin.close();
            } catch (IOException ex) {
            }
        }
     return content;
    }
}