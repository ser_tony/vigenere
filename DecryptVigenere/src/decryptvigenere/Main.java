

package decryptvigenere;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStreamReader;

/**
 *
 * @author tony
 */
public class Main {

    private static boolean isDebug = false;
    private static String defaultFilePath = "/home/tony/NetBeansProjects/DecryptVigenere/test/files/big_text_encrypted.txt";

    /**
     * @param args the command line arguments
     */
    /**
     * This is console application which decrypt ciphertext encrypted
     * by Vigenere cipher and return result to console out:
     * @param args[0] - file path to decrypt text file
     * @output 1: decrypted plain text 2: key
     */
    public static void main(String[] args) throws FileNotFoundException, Exception {
        
        try{
           if (!Main.isDebug && (args.length < 1 || args[0].equals(""))) {
               throw new Exception("You should passing 1 parameter" +
                                   ": \n\r 0 - <file_path>\n\r");
           }
           String filePath = !Main.isDebug? args[0]: Main.defaultFilePath;

           BufferedReader fin = null;
           fin = new BufferedReader(new InputStreamReader(new
                   FileInputStream(filePath)));
           
           String content = "",
                   line = "";
           while ((line = fin.readLine()) != null) {
               content += line;
           }
           fin.close();

           HackVigenere hack = new HackVigenere(content);
           String[] result = hack.hackText(3, 12);
           
           System.out.println("Plain text:\n\r" + result[0]);
           System.out.println("\n\rKey: " + result[1]);

        }  catch (Exception ex){
            System.out.println(ex);
            System.out.println(ex.getStackTrace());
            System.exit(1);
        }
    }
}